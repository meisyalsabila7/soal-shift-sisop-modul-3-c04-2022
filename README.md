# Soal-Shift-Sisop-Modul-2-C04-2022 

### Anggota Kelompok
* Meisya Salsabila Indrijo Putri  5025201114
* Muhammad Haikal Aria Sakti 05111940000088
* Marcelino Salim 5025201026

# Soal 1
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.
### 1.A
Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip `quote.zip` dan music untuk file zip `music.zip`. Unzip ini dilakukan dengan bersamaan menggunakan thread.
* Download
```
char *argv[] = {"wget", "-q", "--no-check-certificate", links[i], "-O", name, NULL};
execv("/usr/bin/wget",argv);
```
* Unzip dan membuat folder
```
char *argv[] = {"mkdir", "-p", names[i],NULL};
execv("/usr/bin/mkdir", argv);
...
char *argv[] = {"unzip", "-qq", name, "-d", names[i],NULL};
execv("/usr/bin/unzip", argv);
```
Lalu fungsi `unzip` dipanggil pada `main`.
```
pthread_create(&threads[0], NULL, unzip, (void *) &int_threads[0]);
pthread_create(&threads[1], NULL, unzip, (void *) &int_threads[1]);
for(int i = 0; i < 2; i++) pthread_join(threads[i], NULL);
```
### 1.B
Decode semua `file .txt` yang ada dengan base 64 dan masukkan hasilnya dalam satu `file .txt` yang baru untuk masing-masing folder (Hasilnya nanti ada dua `file .txt`) pada saat yang sama dengan menggunakan thread dan dengan nama `quote.txt` dan `music.txt`. Masing-masing kalimat dipisahkan dengan `newline/enterq`. Pertama mebuat fungsi `decode` seperti di bawah ini:
```
void *decode(void *index){
	int i = *((int*) index);
	char name[strlen(names[i]) + strlen(".txt") + 1];
	
	strcpy(name, names[i]); 
	strcat(name, ".txt");

	FILE *output = fopen(name, "w");
	int link[2];
	
	DIR *dir_scan;
	struct dirent *entry;
	
	if((dir_scan = opendir(names[i])) == NULL) exit(EXIT_FAILURE);
	while((entry = readdir(dir_scan)) != NULL){
		if(strcmp(".",entry->d_name) == 0 || strcmp("..", entry->d_name) == 0) 
			continue;
		
		char *foo = (char*) malloc(sizeof(char) * 4096);
		
		if(pipe(link) == -1) exit(EXIT_FAILURE);
		pid_t new_id = fork();
		
		if(new_id < 0) exit(EXIT_FAILURE);
		if(new_id == 0){
			char temp[strlen(entry->d_name) + strlen(names[i]) + 2];
			strcpy(temp, names[i]);
			strcat(temp,"/");
			strcat(temp, entry->d_name);
			
			dup2(link[1], STDOUT_FILENO);
			close(link[0]);
			close(link[1]);
			
			char *argv[] = {"base64", "-d", temp, NULL};
			execv("/usr/bin/base64", argv);
		}
		waitpid(new_id,NULL,0);
		close(link[1]);
		read(link[0],foo,4096);
		fprintf(output, "%s\n",foo);
	}
	fclose(output);
}
```
Lalu fungsi tersebut dilakukan pemanggilan pada `main`.
```
pthread_create(&threads[0], NULL, decode, (void *) &int_threads[0]);
pthread_create(&threads[1], NULL, decode, (void *) &int_threads[1]);
for(int i = 0; i < 2; i++) pthread_join(threads[i], NULL);
```
### 1.C
Pindahkan kedua `file .txt` yang berisi hasil decoding ke folder yang baru bernama hasil.
```
void hasil(){
	...
		char *argv[] = {"mkdir", "-p", "hasil", NULL};
		execv("/usr/bin/mkdir", argv);
    ...
	//moving hasil to hasil directory
	for(int i = 0; i < 2; i++){
		char *name = (char*) malloc(strlen(names[i]) + strlen(".txt") + 1);
		strcpy(name, names[i]); 
		strcat(name, ".txt");
		
		char *dirtemp = (char*) malloc(strlen(cur_dir) + strlen("hasil") + 2);
		strcpy(dirtemp, cur_dir);
		strcat(dirtemp,"/");
		strcat(dirtemp,"hasil");
		
		new_id = fork();
		if(new_id < 0) exit(EXIT_FAILURE);
		if(new_id == 0){
			char *argv[] = {"mv", name, dirtemp, NULL};
			execv("/usr/bin/mv", argv);
		}
		waitpid(new_id,NULL,0);
	}
}
```
### 1.D
Folder hasil di-zip menjadi file `hasil.zip` dengan password `'mihinomenest[Nama user]'`. (contoh password : mihinomenestnovak)
```
char *argv[] = {"zip", "-q", "-P", password, "-r", "hasil.zip", "hasil", NULL};
execv("/usr/bin/zip", argv);
```
Dilakukan pemanggilan pada `main`.
```
char password[strlen(pw->pw_name) + strlen("mihinomenest") + 1];
strcpy(password, "mihinomenest");
strcat(password, pw->pw_name);
	
zip_hasil(password); 
```
### 1.E
Karena ada yang kurang, kalian diminta untuk unzip file `hasil.zip` dan buat file `no.txt` dengan tulisan `'No'` pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.
```
void *no_txt(){
	...
	char *dirtemp = (char*) malloc(strlen(cur_dir) + strlen("hasil") + 2);
	strcpy(dirtemp, cur_dir);
	strcat(dirtemp,"/");
	strcat(dirtemp,"hasil");
	...
		char *argv[] = {"mv","no.txt", dirtemp, NULL};
		execv("/usr/bin/mv", argv);
	...
}
```
Pemanggilan pada fungsi `main`.
```
pthread_create(&threads[0], NULL, no_txt, NULL);
pthread_create(&threads[1], NULL, unzip_hasil, (void*)password);
```
### Kendala dan Dokumentasi
* Kendala:
Sempat terkendala dikarenakan kurangnya mengerti cara penggunaan `pthread`.
* Dokumentasi

![Screenshot_from_2022-04-17_20-01-16](/uploads/7a091bc14f71a6936fb4afb8e8e32c0f/Screenshot_from_2022-04-17_20-01-16.png)
![Screenshot_from_2022-04-17_20-02-19](/uploads/35fe42d9f93bd9cd050c0e5cac2f9933/Screenshot_from_2022-04-17_20-02-19.png)
![Screenshot_from_2022-04-17_20-02-49](/uploads/fc21db81047a672678ba6a147ae05c32/Screenshot_from_2022-04-17_20-02-49.png)


# Soal 2
Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:
### 2.A
Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file `users.txt` dengan format `username:password`. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di `users.txt` yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:
* Username unique (tidak boleh ada user yang memiliki username yang sama)
* Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil
```
/* Server Side */
void auth_handler(int new_socket){
    char choice[sz10b];
    valread = read(new_socket , choice, sz10b);
    if (choice[0] == 'r'){
        regis_handler(new_socket);
    }else if (choice[0] == 'l'){
        login_handler(new_socket);
    }
}
```
Dilakuakan pemilihan request dari client yang kemudian diterima server.

```
/* Client Side */
void auth(){
    char choice[1];
    printf("type r for register, l for login\n");
    scanf("%s", choice);
    printf("Dont type anything, wait until another client disconnect!\n");
    write(sock, choice, sz10b);
    if (choice[0] == 'r'){
        regist();
    }else if (choice[0] == 'l'){
        login();
    }
}
```
Dilakuakan pemilihan request client yang akan dikirim ke server.
```
/* Server side */
void regis_handler(int new_socket){
    char line[sz10b], data_user[sz10b][sz]={};
    int idx=0;
    FILE* fp = fopen(users_file,"r");
	if(!fp) {
        printf("Unable to open %s\n", users_file);
    }else {
        printf("Username List:\n");
        while (fgets(line, sizeof(line), fp)) {
            char *u;
            const char s[2] = ":";
            u = strtok(line, s);
            printf("-%s\n", u);
            strcpy(data_user[idx], u);
            idx++;
        }
        fclose(fp);
    }
    char temp_buff[sz10b]={};
    sprintf(temp_buff, "%d", idx);
    write(new_socket, temp_buff, sz10b);
    write(new_socket, data_user, sizeof(data_user));

    char user[sz10b]={},
         pass[sz10b]={};

    valread = read(new_socket, user, sz10b);
    valread = read(new_socket, pass, sz10b);
    fp = fopen(users_file,"a");
    fprintf(fp, "%s:%s\n", user, pass);
    printf("User [%s] Registered\n", user);
    fclose(fp);
}
```
Pada server side diatas diambil nama nama username dari file `users.txt` kemudian dikirimkan ke client side untuk dicek jika terdapat client baru yang register dengan username yang sama dengan database user. Kemudian mengambil username dan password dari client untuk disimpan ke database server.
```
/* Client Side */
void regist(){
    bool p_num = true, p_up = true, p_low = true, u_=false;
    char c, user[sz]={}, pass[sz]={}, temp_buff[sz10b]={};
    char data_user[sz10b][sz]={};
    int idx, i;
    valread = read(sock, temp_buff, sz10b);
    idx = string_to_int(temp_buff);
    valread = read(sock, data_user, sizeof(data_user));
    printf("Server free, you may type now\n");
    printf("Username: ");
    while(!u_){
        scanf("%s", user);
        for (i = 0; i < idx; i++){
            if (!strcmp(data_user[i], user)){
                u_=true;
                break;
            }
        }
        if (u_){
            u_ = false;
            printf("Username already exist\nUsername: ");
        }else {
            u_ = true;
            getchar();
        }
    }

    printf("Password: ");
    while(p_num || p_low || p_up) {
        c = getchar();
        while (c > 31){
            if (c == 32) {c = getchar(); continue;}
            if (p_num == true && c>47 && c<58) p_num = false;
            if (p_up == true && c>64 && c<91) p_up = false;
            if (p_low == true && c>96 && c<123) p_low = false;
            strncat(pass, &c, 1);
            c = getchar();
        }
        if(p_num || p_low || p_up || strlen(pass) < 6){
            printf("\nPassword Incorrect, password must contain the following:\n");
            if (strlen(pass) < 6) printf("- Minimum 6 characters\n");
            if (p_num) printf("- A  number\n");
            if (p_low) printf("- A  lowercase letter\n");
            if (p_up) printf("- An uppercase letter\n");
            p_num = p_up = p_low = true;
            memset(pass, '\0', sz);
            printf("Password: ");
        }
    }
    write(sock, user, sz10b);
    write(sock, pass, sz10b);
}
```
Pada client side akan mengambil data user dari database server untuk sebagai pengecekan nilai unique untuk username. Untuk password terdapat pengecekan sesuai permintaan soal jika sudah sesuai maka username dan password akan dikirim ke server.
### 2.B
Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama `problems.tsv` yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan `\t`. File otomatis dibuat saat server dijalankan.
```
/* Server Side */
void make_tsv(){
    FILE* fp;
    if (fp = fopen(database, "r")){
    }else{
        fp = fopen(database, "a");
        fprintf(fp, "Judul\tAuthor\n");
    }
    fclose(fp);
}
void *client_connect_handler(void *sock){
    ...
    make_tsv();
    ...
}
```
Pada `make_tsv()` dilakukan pengecekan file database terlebih dahulu, jika belum ada maka dibuat dan dilakukan pembuatan kolom. Jika sudah ada maka dibiarkan.
### 2.C
Client yang telah login, dapat memasukkan command yaitu `‘add’` yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:
* Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
* Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
* Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
* Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)
```
/* Server Side */
int command_handler(int new_socket){
    ...
    valread = read(new_socket, com, sizeof(com));
    if (!strcmp(com, "add")){
        printf("User [%s] adding a problem\n", userlog);
        FILE* fp = fopen(database, "a");
        char question[4][sz] = {"Judul problem:", "Filepath description.txt:", "Filepath input.txt:", "Filepath output.txt:"};
        write2d(question, 4, new_socket);
        char serv_path[sz*2], client_path[sz*2], prob[sz]={};
        read(new_socket, client_path, sizeof(client_path));
        for(int i=0; i<4; i++){
            char buffer[sz10b]={}, src_path[sz10b]={}, dest_path[sz10b]={};
            valread = read(new_socket, buffer, sizeof(buffer));
            if (i == 0){
                struct stat st = {};
                while (stat(buffer, &st) != -1) {
                    write(new_socket, "error", sz);
                    valread = read(new_socket, buffer, sizeof(buffer));
                }
                if (stat(buffer, &st) == -1) write(new_socket, "success", sz);
                fprintf(fp, "%s\t%s\n", buffer, userlog);
                strcpy(prob, buffer);
                mkdir(buffer, 0700);
                chdir(buffer);
                if (getcwd(serv_path, 100*2) == NULL){
                    perror("error\n");
                }
            }else{
                strcat(dest_path, serv_path);
                strcat(dest_path, "/");
                strcat(dest_path, buffer);
                strcat(src_path, client_path);
                strcat(src_path, "/");
                strcat(src_path, buffer);
                download_file(src_path, dest_path);
            }
        }
        chdir("..");
        printf("Problem [%s] has been added by [%s]\n", prob, userlog);
        fclose(fp);
        return 1;
    }
    ...
}
```
Pada server akan mengambil command jika commandnya `add` maka pertanyaan akan dilemparkan ke client dan kemudian diambil jawabannya, setelah jawab diambil file file yang di upload client akan di masukkan ke dalam suatu problem dengan nama judul problem yang diinput client. Setelah itu nama dan author pembuat problem akan di ketik di file `problem.tsv`.
```
/* Client Side */
int command(){
    ...
    write(sock, com, sizeof(com));
    if (!strcmp(com, "add")){
        char question[4][sz]={0};
        char client_dir[sz*2];
        getcwd(client_dir, sz*2);
        read2d(question, 4);
        write(sock, client_dir, sizeof(client_dir));
        for(int i=0; i<4; i++){
            char in[sz]={}, path[sz]={};
            printf("%s ", question[i]);
            scanf("%s", in);
            write(sock, in, sizeof(in));
            if (i==0){
                char temp[sz]={};
                read(sock, temp, sz);
                while(!(strcmp(temp, "error"))){
                    printf("Problem exist!\n%s ", question[i]);
                    scanf("%s", in);
                    write(sock, in, sizeof(in));
                    read(sock, temp, sz);
                }
            }
        }
        printf("\n");
        return 1;
    }
    ...
}
```
Pada client akan memasukan jawaban sesuai pertanyaan dari server kemudian jawab dikirimkan ke server dengan kriteria nama problem tidak boleh sama dengan yang sudah ada di server.
### 2.D
Client yang telah login, dapat memasukkan command `‘see’` yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:
```
/* Server Side */
int command_handler(int new_socket){
    ...
    valread = read(new_socket, com, sizeof(com));
    ...
    else if (!strcmp(com, "see")){
        printf("User [%s] see problem list\n", userlog);
        FILE* fp = fopen(database, "r");
        char author[sz10b][sz]={}, problem[sz10b][sz]={};
        fscanf(fp, "%s %s\n", problem[0], author[0]);
        int idx = 0;
        while(fscanf(fp, "%s\t%s", problem[idx], author[idx]) == 2){
            idx++;
        }
        writeInt(idx, sz, new_socket);
        for(int i=0; i<idx; i++){
            write(new_socket, problem[i], sz);
            write(new_socket, author[i], sz);
        }
        fclose(fp);
        return 1;
    }
    ...
}
```
Pada server akan membuka file `problem.tsv` dan membaca semua isinya kemudian datanya di lempar ke client.
```
/* Client Side */
int command(){
    ...
    write(sock, com, sizeof(com));
    ...
    else if (!strcmp(com, "see")){
        char author[sz], problem[sz], buffer[sz];
        printf("Problem list:\n");
        valread = read(sock, buffer, sz);
        int idx = string_to_int(buffer);
        for(int i=0; i<idx; i++){
            read(sock, problem, sz);
            read(sock, author, sz);
            printf("-> %s by %s\n", problem, author);
        }
        printf("\n");
        return 1;
    }
}
```
Pada client semua data yang di ambil dari server akan di print sesuai format yang diminta.
### 2.E
Client yang telah login, dapat memasukkan command `‘download <judul-problem>’` yang berguna untuk mendownload file `description.txt` dan `input.txt` yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu `<judul-problem>`. Kedua file tersebut akan disimpan ke folder dengan nama `<judul-problem>` di client.
```
/* Server Side */
void download_file(char *src_path, char *dst_path){
    int src_fd, dst_fd, n, err;
    unsigned char buffer[sz10b*10];

    src_fd = open(src_path, O_RDONLY);
    dst_fd = open(dst_path, O_CREAT | O_WRONLY);

    while (1) {
        err = read(src_fd, buffer, sz10b*10);
        if (err == -1) {
            printf("Error reading file.\n");
            exit(1);
        }
        n = err;

        if (n == 0) break;

        err = write(dst_fd, buffer, n);
        if (err == -1) {
            printf("Error writing to file.\n");
            exit(1);
        }
    }
    close(src_fd);
    close(dst_fd);
}
int command_handler(int new_socket){
    ...
    valread = read(new_socket, com, sizeof(com));
    ...
    else if (!strcmp(com, "download")){
        printf("User [%s] download a problem\n", userlog);
        char problem[sz]={}, client_path[sz*2]={}, desc_path[sz*2]={}, in_path[sz*2]={}, serv_path[sz*2]={}, i_c_path[sz*2]={}, d_c_path[sz*2]={}, message[sz10b]={};
        FILE* fp;
        valread = read(new_socket, problem, sz);
        valread = read(new_socket, client_path, sz*2);
        getcwd(serv_path, sz*2);
        strcat(desc_path, serv_path);
        strcat(desc_path, "/");
        strcat(desc_path, problem);
        strcat(desc_path, "/");
        strcat(in_path, desc_path);
        strcat(desc_path, "description.txt");
        strcat(in_path, "input.txt");
        if((fp = fopen(in_path, "r")) == NULL){
            strcpy(message, "Problem doesn't exist!");
            write(new_socket, message, sz10b);
            return 1;
        }
        strcat(client_path, "/");
        strcat(client_path, problem);
        mkdir(client_path, 0700);
        strcat(i_c_path, client_path);
        strcat(i_c_path, "/input.txt");
        strcat(d_c_path, client_path);
        strcat(d_c_path, "/description.txt");
        if((fp = fopen(i_c_path, "r")) != NULL){
            fclose(fp);
            strcpy(message, "Problem has been already downloaded!");
            write(new_socket, message, sz10b);
            return 1;
        }
        download_file(in_path, i_c_path);
        download_file(desc_path, d_c_path);
        strcpy(message, "Problem successfully downloaded!");
        write(new_socket, message, sz10b);
        printf("Problem [%s] successfully downloaded by [%s]\n", problem, userlog);
        return 1;
    }
    ...
}
```
Pada server ketika client memberikan request `download`, server akan mengambil path client kemudian dilakukan exception jika problem yang diminta ada atau tidak dan juga jika tenyata client telah memiliki problem. Pada `download_file` dilakukan pembacaan sampai size yang dikirim ialah 0 atau pembacaan sudah tidak ada lagi.
```
/* Client Side */
int command(){
    ...
    write(sock, com, sizeof(com));
    ...
    else if (!strcmp(com, "download")){
        char problem[sz]={}, path[sz*2]={}, message[sz10b]={};
        scanf("%s", problem);
        write(sock, problem, sz);
        getcwd(path, sz*2);
        write(sock, path, sz*2);
        valread = read(sock, message, sz10b);
        printf("%s\n\n", message);
        return 1;
    }
}
```
Pada client diambil pathnya kemudian dikirim ke server side. Setelah pemrosesan pada server akan dikirim feedback message berhasil/tidak nya mendownload problem.
### 2.F
Client yang telah login, dapat memasukkan command `‘submit <judul-problem> <path-file-output.txt>’`. Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file `output.txt` nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file `output.txt` yang telah dikirimkan oleh client dan `output.txt` yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan `“AC”` dan jika tidak maka server akan mengeluarkan pesan `“WA”`.
```
/* Server Side */
int command_handler(int new_socket){
    ...
    valread = read(new_socket, com, sizeof(com));
    ...
    else if (!strcmp(com, "submit")){
        printf("User [%s] submit a problem\n", userlog);
        char problem[sz]={}, path[sz*2]={}, curr_path[sz*2]={};
        valread = read(new_socket, problem, sz);
        valread = read(new_socket, path, sz*2);
        getcwd(curr_path, sz*2);
        strcat(curr_path, "/");
        strcat(curr_path, problem);
        strcat(curr_path, "/");
        strcat(curr_path, "output.txt");
        FILE* fp = fopen(curr_path,"r");
        if(!fp) {
            printf("Unable to open %s\n", curr_path);
            exit(0);
        }
        char serv_out[sz10b][sz10b]={}, cli_out[sz10b][sz10b]={}, line[sz10b]={};
        int idx_serv=0, idx_cli=0;
        while (fgets(line, sizeof(line), fp)) {
            char *u;
            u = strtok(line, "\n");
            strcpy(serv_out[idx_serv], u);
            idx_serv++;
        }fclose(fp);
        fp = fopen(path,"r");
        if(!fp) {
            printf("Unable to open %s\n", path);
            exit(0);
        }
        while (fgets(line, sizeof(line), fp)) {
            char *u;
            u = strtok(line, "\n");
            strcpy(cli_out[idx_cli], u);
            idx_cli++;
        }fclose(fp);
        char message[sz]={};
        if(idx_serv != idx_cli) {
            strcpy(message, "WA");
            write(new_socket, message, sz);
            return 1;
        }
        for (int i=0; i<idx_serv; i++){
            printf("-%s-%s-\n", serv_out[i], cli_out[i]);
            if(strcmp(serv_out[i], cli_out[i])){
                strcpy(message, "WA");
                write(new_socket, message, sz);
                return 1;
            }
        }
        strcpy(message, "AC");
        write(new_socket, message, sz);
        return 1;
    }
    ...
}
```
Pada server side akan diambil isi dari `output.txt` client dan server. Kemudian pengecekan pertama pada index yang merupakan baris dari file jika banyak barisnya sudah berbeda antara `output.txt` server dan client maka akan dikirimkan message `WA` kepada client. Kemudian jika banyak baris sudah sama, selanjutnya akan dilakukan pengecekan isi string perbaris. Jika ada 1 yang berbeda maka akan dikirimkan message `WA` kepada client. Namun jika ternyata isinya sama maka akan dikirimkan message `AC`.
```
/* Client Side */
int command(){
    ...
    write(sock, com, sizeof(com));
    ...
    else if (!strcmp(com, "submit")){
        char problem[sz]={}, path[sz*2]={}, curr_path[sz*2], message[sz];
        scanf("%s%s", problem, path);
        getcwd(curr_path, sz*2);
        write(sock, problem, sz);
        strcat(curr_path, "/");
        strcat(curr_path, path);
        write(sock, curr_path, sz*2);
        valread = read(sock, message, sz);
        printf("%s\n\n", message);
        return 1;
    }
}
```
Pada client side akan diambil path dari output yang disubmit kemudian dikirimkan ke server. Jika pemrosesan pada server telah selesai, akan dikirimkan feedback message dari server.
### 2.G
Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.
```
/* Server Side */
int main(){
    ...
    while((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))){
        printf("Connection accepted\n");

        pthread_t sniffer_thread;
		int *new_sock = malloc(1);
		*new_sock = new_socket;

		if( pthread_create( &sniffer_thread , NULL ,  client_connect_handler , (void*) new_sock) < 0){
			perror("could not create thread");
			return 1;
		}

		pthread_join(sniffer_thread ,NULL);
		printf("\nDisconnected from client\n");
        printf("Waiting for client connections...\n");
    }
    ...
}
```
Untuk setiap koneksi akan diambil menggunakan `accept()` function. Untuk pemrosesan menggunakan thread dengan inisialisasi `pthread_create()` function, kemudian pada soal diminta untuk melakukan handle client satu per satu sehingga menggunakan `pthread_join()` function untuk menunggu hingga satu clinet / satu thread selesai.
### Dokumentasi:
* 2.A dan 2.G
[![image.png](https://i.postimg.cc/PJPdMbht/image.png)](https://postimg.cc/1V1xmqfT)
![image](/uploads/fd32df02fb1f955e35ba4550732c04ee/image.png)

* 2.B
![image](/uploads/444f889cc9a72d9c8703f782d58a29d6/image.png)

* 2.C
![image](/uploads/2232b4f9790cec0c9912a8225e0d2343/image.png)

* 2.D
![image](/uploads/3269884048267afc72ee17f6eb212a94/image.png)

* 2.E
![image](/uploads/daf82b6d882b3d2fc6a3a6b69d83bbf1/image.png)

* 2.F
![image](/uploads/1da56713154259b85a912e1cffbb4b84/image.png)

### Kendala
1. Kurang bisa mengatur jalannya thread. Misalnya ingin menjalankan thread 3 dan 4 hanya ketika thread 1 dan 2 selesai dijalankan. Akhirnya untuk nomor 1 diakalin menggunakan sleep untuk menunda thread selanjutnya dijalankan dan menunggu thread sebelumnya selesai sepenuhnya.
2. Pernah error saat pengiriman antara server dan client menggunakan write dan read. Ternyata, ketika mengirim suatu data ke socket, melakukan pembacaan harus sesuai dengan size pengiriman sehingga tidak ada data yang tertinggal maupun terpotong.


# Soal 3
Pada soal no 3 ini, diminta untuk mencoba merapikan file bernama harta karun berdasarkan `jenis/tipe/kategori/ekstensinya`. mengextract zip yang diberikan dalam folder `/home/[user]/shift3/`. Lalu working directory program akan berada dalam folder `/home/[user]/shift3/hartakarun/`. Semua file harus berada dalam folder, jika terdapat file yang tidak memiliki eksistensi, maka file akan disimpan dalam folder `unknown` jika file hidden makan akan disimpan dalam folder `hidden`. Agara proses kategori dapat berjalan dengan cepat, maka setiap 1 file yang dikategorikan akan dioperasikan oleh 1 thread. Nami menggunakan program client-server. Saat program `client` dijalan kan , maka folder `/home/[user]/shift3/hartakarun/` akan di-zip terlebih dhulu. `client` dapat mengirim file `hartakarun.zip` ke server.
### 3.A
Pertama akan membuat void extract, dengan menggunakan fungsi `popen` dengan argumen pertamaya adalah value dan argumen kedua adalah r.
```
void extract(char *value) 
{
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer,'\0',sizeof(buffer));

    file = popen(value ,"r");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n",buffer);
        pclose(file);
    }
}
```
### 3.B
Pertama akan membuat `void sortfile` untuk mengkategorikan file.
```
void sortFile(char *from)
{
    struct_path s_path;
    struct dirent *dp;
    DIR *dir;

    flag = 1;
    int index = 0;
    strcpy(s_path.cwd, "/home/theresianwg/shift3/hartakarun");

    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move, (void *)&s_path);
            sleep(1);
            index++;
        }
    }
}
```
Lalu program akan membaca directory hasil extract file.zip dan akan dicek satu-satu. Untuk informasi mengenai directory asal dan nama file akan disimpan pada variabel `filename`.
### 3.C
Pada kali ini menggunakan fungsi `sortfile` dan memamnggil fungsi `move`.
```
pthread_create(&tid[index], NULL, move, (void *)&s_path);
```
Lalu menggunakan
```
void *move(void *s_path)
{
    struct_path s_paths = *(struct_path*) s_path;
    moveFile(s_paths.from, s_paths.cwd);
    pthread_exit(0);
}
```
dan dilanjutkan dengan fungsi `movefile`,
```
void moveFile(char *p_from, char *p_cwd)
{
    ...
    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }

    toLower(file_ext);
    ...
    // information
    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    // create dir and move file
    createDir(file_ext);
    rename(p_from, p_cwd);
}
```
Dengan bantuan variabel `buffer` yang sebelumnya akan diisi nama file dengan bantuan fungsi `strtok` untum mencari delimiter dengn `/`. Lalu, menentukan eksistensi file dengan bantuan variabel `buffer`  yang berisi formt file stelah diinisiasi hasil dari `strtok` dengan delimiter `.`. Tetapi, terdapat pengecualian jika `NULL` maka artinya file dikategorikan hidden dan jika tidak memenehui keduannya akan dikategorikan `unkown`. Dilanjutkan dengan membuat destinasi dari file, juga menampilkan informasi file dan membuat directory berdasarkan eksistensi filenya.
### 3.D
Disini akan menggunakan fungsi `zip` yang akan menerima parameter dengan berupa command yang dieksekusi pada terminal. Command ini selanjutnya akan dieksekusi dengan popen yang ada dalam fungsi `zip`.
```
void zip(char *ziping)
{
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer, '\0', sizeof(buffer));

    //ziping
    file = popen(ziping, "w");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n", buffer);
        pclose(file);
    }
}
```
### Dokumentasi
![Screenshot_from_2022-04-17_21-14-25](/uploads/607b7d2a09e8538ed1e0cb7a7c860139/Screenshot_from_2022-04-17_21-14-25.png)
![Screenshot_from_2022-04-17_21-15-00](/uploads/4a0de9ff96219de4438c72d7811bd936/Screenshot_from_2022-04-17_21-15-00.png)
